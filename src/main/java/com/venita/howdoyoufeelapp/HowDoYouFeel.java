/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.venita.howdoyoufeelapp;

import java.io.Serializable;

/**
 *
 * @author User
 */

public class HowDoYouFeel implements Serializable{

    public HowDoYouFeel(String Feel, int Date, String description) {
        this.Feel = Feel;
        this.Date = Date;
        this.description = description;
    }

    HowDoYouFeel(int Date, String Feel, String Description) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getFeel() {
        return Feel;
    }

    public int getDate() {
        return Date;
    }

    public String getDescription() {
        return description;
    }

    public void setFeel(String Feel) {
        this.Feel = Feel;
    }

    public void setDate(int Date) {
        this.Date = Date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("HowDoYouFeel{");
        sb.append("Feel=").append(Feel);
        sb.append(", Date=").append(Date);
        sb.append(", description=").append(description);
        sb.append('}');
        return sb.toString();
    }
    private String Feel ;
    private int Date;
    private String description;
    
}
